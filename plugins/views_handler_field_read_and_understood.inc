<?php

class views_handler_field_read_and_understood extends views_handler_field {
  function construct() {
    parent::construct();
  }
  function render($values) {
    return check_plain($values->{$this->field_alias});
  }
}