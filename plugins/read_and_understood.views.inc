<?php

/**
 * @file
 * Views specific include file for implementing Read and Understood in Views.
 */

/**
 * Implementation of hook_views_handlers().
 */
function read_and_understood_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'read_and_understood') .'/plugins',
    ),
    'handlers' => array(
      'views_handler_field_read_and_understood' => array(
        'parent' => 'views_handler_field',
        'path' => drupal_get_path('module', 'read_and_understood') .'/plugins',
      ),
    )
  );
} // read_and_understood_views_handlers

/**
 * Implementation of hook_views_data().
 */
function read_and_understood_views_data() {
	$data['read_and_understood']['table']['group']  = t('Read and Understood');
	$data['read_and_understood']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Read and Understood'),
    'help' => t("Whether a user has read and understood the content of a particular node."),
  );
  $data['read_and_understood']['table']['join'] = array(
    'node' => array(
      'type' => 'INNER',
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    'node_revisions' => array(
      'type' => 'INNER',
      'left_field' => 'vid',
      'field' => 'vid',
    ),
    'users' => array(
      'type' => 'INNER',
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  $data['read_and_understood']['user_list'] = array(
    'title' => t('User uid'),
    'help' => t('Which users have confirmed they have read and understood the node content.'),
    'field' => array(
      'field' => 'uid',
      'handler' => 'views_handler_field_read_and_understood',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_argument_user_uid',
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['read_and_understood']['vid'] = array(
    'title' => t('Node'),
    'help' => t('The particular node the "read and understood" is attached to'),
    'relationship' => array(
      'label' => t('Read and Understood: vid'),
      'base' => 'node',
      'base field' => 'vid',
      'skip base' => array('node', 'node_revisions'),
    ),
  );
  $data['read_and_understood']['uid'] = array(
    'title' => t('Users'),
    'help' => t('The users referenced by "Read and Understood"'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
      'skip base' => array('users', 'node', 'node_revision'),
    ),
    'relationship' => array(
      'label' => t('Read and Understood: uid'),
      'base' => 'users',
      'base field' => 'uid',
      'skip base' => array('users'),
    ),
  );
	return $data;
} // read_and_understood_views_data

/**
 * Implementation of hook_views_data_alter()
 */
function read_and_understood_views_data_alter(&$data) {
  $data['node']['read_and_understood'] = array(
    'group' => t('Read and Understood'),
    'title' => t('Node'),
    'help' => t('The node relating to Read and Understand.'),
    'real field' => 'nid',
    'field' => array(
      'handler' => 'views_handler_field_read_and_understood',
    ),
    'relationship' => array(
      'title' => t('Node'),
      'help' => t('Restrict the user list to only show from a certain {node}.nid.'),
      'relationship table' => 'read_and_understood',
      'relationship field' => 'nid',
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Read and Understood: nid'),
    ),
  );
}