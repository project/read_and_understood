
Drupal.behaviors.readandUnderstoodLock = function (context) {
  $('input[name=read_and_understood_collect]').bind('click', function() {
    if ($(this).attr('checked')) {
      $(this).attr('disabled', true);
    }
  });
}