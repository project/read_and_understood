
Drupal.behaviors.readandUnderstood = function (context) {
  $('input#edit-read-and-understood-submit').hide();
  $(".read_and_understood .read_and_understood-select").click(function(event) {
    $(this).parent().addClass("selected");
    $(this).parent().find(":checkbox").attr("checked", "checked");
    $('input[name=read_and_understood_collect]').trigger("change");
    return false;
  });
  $(".read_and_understood .read_and_understood-deselect").click(function(event) {
    $(this).parent().removeClass("selected");
    $(this).parent().find(":checkbox").removeAttr("checked");
    $('input[name=read_and_understood_collect]').trigger("change");
    return false;
  });
  $('input[name=read_and_understood_collect]').change(function() {
    $.get(Drupal.settings.basePath + 'read_and_understood/' + parseInt($('input[name=read_and_understood_nid]').val()) + '/' + parseInt($('input[name=read_and_understood_vid]').val()) + '/' + $('input[name=read_and_understood_collect]').is(':checked'));
    return false;
  });
}